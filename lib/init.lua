-- chikun :: 2015
-- Load all libraries and shorthand

require(... .. "/class")    -- Classes library
require(... .. "/math")     -- Extend the math library

-- Built-in Libraries
le = love.event
lg = love.graphics
lk = love.keyboard
lm = love.mouse
lw = love.window

-- chikun Libaries
cl = require(... .. "/chikun/lighting")
cm = require(... .. "/chikun/maptool")
cp = require(... .. "/chikun/physics")
cr = require(... .. "/chikun/resources")
