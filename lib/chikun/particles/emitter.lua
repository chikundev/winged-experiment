-- chikun :: 2015
-- Simple particle library :: Emitter


--[[ Creates a new particle emitter.
INPUT: x and y are the coordinates of the new emitter.
OUTPUT: The new particle emitter.]]
Emitter = class(function(emitter, x, y)

    emitter.objects = { }

    emitter.x = x
    emitter.y = y

  end)


function Emitter:create()

end


function Emitter:update(dt)

  for key, object in ipairs(self.objects) do

    object:update(dt)

  end

end


function Emitter:draw()

  for key, object in ipairs(self.objects) do

    object:draw()

  end

end
