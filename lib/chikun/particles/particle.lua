-- chikun :: 2015
-- Simple particle library :: Emitter


--[[ Creates a new particle emitter.
INPUT: x and y are the coordinates of the new emitter.
OUTPUT: The new particle emitter.]]
Particle = class(function(particle, x, y)

    particle.objects = { }

    particle.x = x
    particle.y = y

  end)


function Particle:create() end


function Particle:update(dt) end


function Particle:draw() end
