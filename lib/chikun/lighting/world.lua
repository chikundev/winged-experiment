-- chikun :: 2015
-- Simple lighting library :: World


--[[ Class for light world.
INPUT: Width and height of the room.
       tileWidth and tileHeight are the size of the tiles.
OUTPUT: The new light world.]]
LightWorld = class(function(world, width, height, tileWidth, tileHeight)

    world.ambience = { 0, 0, 0, 255 }
    world.lights = { }
    world.tiles = { }

    world.w = width
    world.h = height

    world.tilesH = (width / tileWidth)
    world.tilesV = (height / tileHeight)

    world.tileW = tileWidth
    world.tileH = tileHeight

  end)


--[[ Add a light to the light world.
INPUT: x and y are the position of the light.
       r is the radius of the light.
       f is the falloff distance of the light.
       col is a table containing the colour of the light.
OUTPUT: The light which was added to the world's light table.]]
function LightWorld:addLight(x, y, r, f, col)

  table.insert(self.lights, Light(x, y, r, f, col))
  return self.lights[#self.lights]

end


--[[ Draws the light world.
INPUT: Nothing.
OUTPUT: Nothing.]]
function LightWorld:draw()

  for key, tile in ipairs(self.tiles) do

    lg.setColor(tile)

    local x, y =
      ((key - 1) % self.tilesH) * self.tileW,
      math.floor((key - 1) / self.tilesH) * self.tileH

    lg.rectangle('fill', x, y, self.tileW, self.tileH)

  end

end


--[[ Resets all light tiles in a world to the ambient colour.
INPUT: Nothing.
OUTPUT: Nothing.]]
function LightWorld:reset()

  for i = 1, self.tilesH * self.tilesV do

    self.tiles[i] = self.ambience

  end

end


--[[ Sets the ambient light for the light world.
INPUT: Red, green, blue, and alpha values.
OUTPUT: Nothing.]]
function LightWorld:setAmbience(r, g, b, a)

  self.ambience = { r, g, b, a }

end


--[[ Updates the light world.
INPUT: Nothing.
OUTPUT: Nothing.]]
function LightWorld:update()

  self:reset()    -- Reset all tiles

  for key, light in ipairs(self.lights) do

    for key, tile in ipairs(self.tiles) do

      local x, y =
        ((key - 1) % self.tilesH) * self.tileW + self.tileW / 2,
        math.floor((key - 1) / self.tilesH) * self.tileH + self.tileH / 2

      local dist = math.clamp(0,
        (math.distance(x, y, light.x, light.y) - light.r) / light.f, 1)

      self.tiles[key] = {
        light.col[1] + (tile[1] - light.col[1]) * dist,
        light.col[2] + (tile[2] - light.col[2]) * dist,
        light.col[3] + (tile[3] - light.col[3]) * dist,
        light.col[4] + (tile[4] - light.col[4]) * dist
      }

    end

  end

end
