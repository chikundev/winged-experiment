-- chikun :: 2015
-- Simple lighting library :: Light


--[[ Class for the light.
INPUT: x and y are the position of the light.
       r is the radius of the light.
       f is the falloff distance of the light.
       col is a table containing the colour of the light.
OUTPUT: The new light.]]
Light = class(function(light, x, y, r, f, col )

    -- Light position
    light.x = x
    light.y = y

    light.r = r   -- Light radius
    light.f = f   -- Light falloff

    -- Light colour
    light.col = col

  end)


--[[ Get the position of a light.
INPUT: Nothing.
OUTPUT: The x and y positions of the light.]]
function Light:getPosition()

  return x, y

end


--[[ Get the x position of a light.
INPUT: Nothing.
OUTPUT: The x position of the light.]]
function Light:getX()

  return x

end


--[[ Get the y position of a light.
INPUT: Nothing.
OUTPUT: The y position of the light.]]
function Light:getY()

  return y

end


--[[ Set the position of a light.
INPUT: x and y are the new position of the light.
OUTPUT: Nothing.]]
function Light:setPosition(x, y)

  self.x = x
  self.y = y

end
