-- chikun :: 2015
-- Simple lighting library

require(... .. "/light")   -- Light functions
require(... .. "/world")   -- Light world functions

return {

  newWorld = function(w, h, tw, th)
    return LightWorld(w, h, tw, th)
  end

}
