-- chikun :: 2015
-- Simple Tiled .lua map tool

Map = class(function(newMap, filename, name)

    -- Load map data from file
    local data = require(filename)

    -- Important tables for our map
    newMap.layers = { }     -- Layers (object and tile)
    newMap.quads = { }      -- Cut-up segments of tiles
    newMap.tilesets = { }   -- All useable tilesets

    -- Number of tiles horizontally and vertically
    newMap.tilesH = data.width
    newMap.tilesV = data.height

    -- Width and height of said tiles
    newMap.tileW = data.tilewidth
    newMap.tileH = data.tileheight

    -- Calculated width and height of map
    newMap.w = (data.width * data.tilewidth)
    newMap.h = (data.height * data.tileheight)

    -- Accept given name of map or use a blank string
    newMap.name = name or ""

  end)

return {

  newMap = function(filename, name)

    return Map(filename, name)

  end

}
