-- chikun :: 2015
-- Simple physics library :: World


--[[ Creates a new physics world.
INPUT: gravity is the speed at which an object's vSpeed will increase.
       terminalVelocity is the maximum speed an object can move.
OUTPUT: The new physics world.]]
PhysicsWorld = class(function(world, gravity, terminalVelocity)

    world.gravity = gravity
    world.terminalVelocity = terminalVelocity
    world.objects = { }

  end)


--[[ Adds a physics circle to the physics world.
INPUT: x, y, and r are the dimensions of the rectangle.
       static determines whether this object moves.
OUTPUT: The circle added to the world.]]
function PhysicsWorld:addCircle(x, y, r, static)

  table.insert(self.objects, Circle(x, y, r, static))
  return self.objects[#self.objects]

end


--[[ Adds a physics rectangle to the physics world.
INPUT: x, y, w, and h are the dimensions of the rectangle.
       static determines whether this object moves.
OUTPUT: The rectangle added to the world.]]
function PhysicsWorld:addRectangle(x, y, w, h, static)

  table.insert(self.objects, Rectangle(x, y, w, h, static))
  return self.objects[#self.objects]

end


--[[ Updates the physics world.
INPUT: Delta time.
OUTPUT: Nothing.]]
function PhysicsWorld:update(dt)

  -- Calculate gravity based on delta time
  local gravity = self.gravity * dt

  -- Iterate through all objects in the world
  for initialKey, object1 in ipairs(self.objects) do

    -- Check that object is not static
    if not object1.static then

      -- Increase vSpeed of object, limiting at terminal velocity
      object1.vSpeed = math.min(object1.vSpeed + gravity,
        self.terminalVelocity)

      -- Move object based on vSpeed
      object1.y = object1.y + object1.vSpeed * dt

      -- Set the object to not still since has been moved
      object1.vStill = false

      -- Iterate through all objects in world again
      for key, object2 in ipairs(self.objects) do

        -- Make sure that this object is not the initial one
        if initialKey ~= key then

          -- Check collision between the two objects
          if cp.check(object1, object2) and object1:is_a(Rectangle) and object2:is_a(Rectangle) then

            -- The middle y position of the second object
            local middleY = object2.y + (object2.h / 2)

            -- Check if initial object is above second object and move it
            if (object1.y + (object1.h / 2) < middleY) then

              object1.y = object2.y - object1.h     -- Above second object

            else

              object1.y = object2.y + object2.h     -- Below second object

            end

            -- Stop the initial object's movement
            object1.vSpeed = 0

            -- The object is vertically still
            object1.vStill = true

          end

        end

      end

      object1.x = object1.x + object1.hSpeed * dt

      object1.hMove = false

    end

  end

end
