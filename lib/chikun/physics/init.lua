-- chikun :: 2015
-- Physics libraries

require(... .. "/bodies")
require(... .. "/world")

return {

  check = function(b1, b2)

    if b1:is_a(Rectangle) and b2:is_a(Circle) then

      local b3 = b1

      b1 = b2
      b2 = b3

    end

    -- Check if two rectangles are colliding
    if b1:is_a(Rectangle) and b2:is_a(Rectangle) then

      return (b1.x < b2.x + b2.w and
              b1.y < b2.y + b2.h and
              b2.x < b1.x + b1.w and
              b2.y < b1.y + b1.h)

    -- Check if a circle and a rectangle are colliding
    elseif b1:is_a(Circle) and b2:is_a(Rectangle) then

      local x, y =
        math.clamp(b2.x, b1.x, b2.x + b2.w),
        math.clamp(b2.y, b1.y, b2.y + b2.h)

      return (math.distance(x, y, b1.x, b1.y) < b1.r)

    -- Check if two circles are colliding
    elseif b1:is_a(Circle) and b2:is_a(Circle) then

      local dist = math.distance(b1.x, b1.x, b2.x, b2.y)

      return (b1.r + b2.r < dist)

    end

  end
}
