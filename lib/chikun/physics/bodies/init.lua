require(... .. "/circle")
require(... .. "/point")
require(... .. "/rectangle")

Body = class(function(object, x, y, static, hSpeed, vSpeed)

    object.x = x
    object.y = y

    object.static = static or false

    object.hMove = false

    object.hSpeed = hSpeed or 0
    object.vSpeed = vSpeed or 0

    object.hStill = true
    object.vStill = true

  end)


function Body:getCentre()

  return self.x, self.y

end


function Body:isStatic()

  return self.static

end


function Body:isHStill()

  return self.hStill

end

function Body:isVStill()

  return self.vStill

end

function Body:addHSpeed(hSpeed)

  self.hMove = true

  self.hSpeed = self.hSpeed + hSpeed

end

function Body:setVSpeed(vSpeed)

  self.vSpeed = vSpeed

end

function Body.inherit(object)

  -- Inherit functions
  object.getCentre = (object.getCentre) or Body.getCentre
  object.isHStill = (object.isHStill) or Body.isHStill
  object.isVStill = (object.isVStill) or Body.isVStill
  object.addHSpeed = (object.addHSpeed) or Body.addHSpeed
  object.setVSpeed = (object.setVSpeed) or Body.setVSpeed

end
