Rectangle = class(Body, function(object, x, y, w, h, static, hSpeed, vSpeed)

    Body.init(object, x, y, static, hSpeed, vSpeed)
    Body.inherit(object)

    object.w = w
    object.h = h

  end)

function Rectangle:getCentre()

  return self.x + (self.w / 2), self.y + (self.h / 2)

end
