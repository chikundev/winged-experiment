Circle = class(Body, function(object, x, y, r, static, hSpeed, vSpeed)

    Body.init(object, x, y, static, hSpeed, vSpeed)
    Body.inherit(object)

    object.r = r

  end)
