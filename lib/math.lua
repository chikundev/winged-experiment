-- chikun :: 2014-15
-- Extra maths functions


--[[ Calculates the angle from one point to another.
INPUT: x1 and y1 are the coordinates of the first point.
       x2 and y2 are the coordinates of the second point.
OUTPUT: The angle from point1 to point 2 in radians.]]
math.angle = function(x1, y1, x2, y2)

  return math.atan2(y2 - y1, x2 - x1)

end


--[[ Constrains a value to a minimum and a maximum.
INPUT: Three numbers, min, the value to be clamped, and the max.
OUTPUT: If the value is smaller than min, returns min.
        If the value is larger then max, returns max.
        Otherwise returns value.]]
math.clamp = function(min, value, max)

  return math.max(min, math.min(max, value))

end


--[[ Calculates the distance from one point to another.
INPUT: x1 and y1 are the coordinates of the first point.
       x2 and y2 are the coordinates of the second point.
OUTPUT: The distance from point1 to point 2.]]
math.distance = function(x1, y1, x2, y2)

  return ((x2 - x1) ^ 2 + (y2 - y1) ^ 2) ^ 0.5

end


--[[ Returns the value of an argument rounded to dec decimal
     places, or 0 if omitted.
INPUT: The value to be rounded and the number of decimal places
       to round to.
OUTPUT: value rounded to dec decimal places.
NOTE: Negative dec results in rounding behind the decimals
      eg. math.round(4777, -2) == 4800]]
math.round = function(value, dec)

  dec = 10 ^ (dec or 0)
  return math.floor(value * dec + 0.5) / dec

end


--[[ Returns the sign of a value.
INPUT: value is the number to be checked.
       preventZero will turn 0 into 1.
OUTPUT: If value is smaller than 0, returns -1.
        If value is larger than 0, returns 1.
        If value is equal to 0, returns 0.]]
math.sign = function(value, preventZero)

  if (value < 0) then

    return -1

  elseif (value > 0) then

    return 1

  else

    if preventZero then

      return 1

    end

    return 0

  end

end
