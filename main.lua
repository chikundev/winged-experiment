-- chikun :: 2015
-- Winged Experiment

require "lib"

math.randomseed(os.time())    -- Seeds the RNG

function love.load()

  view = {
    x = 0, y = 0, w = 256, h = 192
  }

  world = PhysicsWorld(800, 800)

  player = {
    body = world:addRectangle(64, 32, 32, 32, false)
  }
  floor = world:addRectangle(0, 160, 128, 32, true)
  ball = world:addCircle(64, 64, 64, false)

end


function love.update(dt)

  dt = math.min(dt, 1/15)

  world:update(dt)

  player.body:addHSpeed(40 * dt)

  if lk.isDown(' ') and player.body:isVStill() then

    player.body:setVSpeed(-400)

  end

end


function love.draw()

  lg.scale(2)

  lg.setColor(255, 255, 255)

  lg.rectangle('fill', player.body.x, player.body.y, player.body.w, player.body.h)
  lg.rectangle('fill', floor.x, floor.y, floor.w, floor.h)

  lg.origin()

end
