-- chikun :: 2015
-- Winged Experiment Configuration


function love.conf(we)

  we.window.width = 512
  we.window.height = 384

  we.window.title = "Winged Experiment"

  we.console = true

end
